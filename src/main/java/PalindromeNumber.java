public class PalindromeNumber {

    public static void main(String[] args) {
        int actualNum= 12321;
        int num = actualNum;
        int remainder=0;
        int reversenum=0;

        while( num != 0 )
        {
            remainder = num % 10;
            reversenum = reversenum * 10 + remainder;
            num  /= 10;
        }

        if(actualNum == reversenum)
            System.out.println("Palendrome");
        else
            System.out.println("Not a Palendrome");
    }
}
