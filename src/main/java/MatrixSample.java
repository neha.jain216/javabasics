public class MatrixSample {

    public static void main(String[] args) {
        int[][] matrix = { { 11, 12, 13, 14 },
                            { 21, 22, 23, 24 },
                            { 31, 32, 33, 34 } };
        print2D(matrix);

        System.out.println("\nSum of 2D matrix");
        int[][] firstMatrix = { { 11, 12, 13},
                { 21, 22, 23 },
                { 31, 32, 33 } };

        int[][] secondMatrix = { { 11, 12, 13},
                { 21, 22, 23 },
                { 31, 32, 33 } };

        print2D(add2D(firstMatrix,secondMatrix));

        System.out.println("\n Multiplication of 2D Matrix");

        int[][] fMatrix = { {3, 2, 5}, {3, 0, 4} };
        int[][] sMatrix = { {2, 3}, {2, 0}, {0, 1} };
        print2D(multiply2D(fMatrix,sMatrix));

    }

    public static void print2D(int[][] matrix)
    {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static int[][] add2D(int[][] firstMatrix, int[][] secondMatrix)
    {
        // Adding Two matrices
        int rows = 3;
        int columns  = 3;
        int[][] sum = new int[rows][columns];
        for(int i = 0; i < firstMatrix.length; i++) {
            for (int j = 0; j < firstMatrix[i].length; j++) {
                sum[i][j] = firstMatrix[i][j] + secondMatrix[i][j];
            }
        }
        return sum;
    }

    public static int[][] multiply2D(int[][] firstMatrix, int[][] secondMatrix)
    {
        int r1 = 2, c1 = 3;
        int r2 = 3, c2 = 2;

        int[][] product = new int[r1][c2];
        for(int i = 0; i < r1; i++) {
            for (int j = 0; j < c2; j++) {
                for (int k = 0; k < c1; k++) {
                    product[i][j] += firstMatrix[i][k] * secondMatrix[k][j];
                }
            }
        }
        return  product;
    }
}
