import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class LinkedListSample {
    public static void main(String[] args) {
        List<Integer> numbers = new LinkedList<>();
        numbers.add(10);
        numbers.add(6);
        numbers.add(21);
        numbers.add(33);
        numbers.add(56);
        numbers.add(6);

        numbers.remove(2);
        numbers.add(2,100);
        Collections.sort(numbers);
        System.out.println(numbers+"---");

//        for (Integer num: numbers)
//        {
//            System.out.println(num);
//        }

        Iterator itr = numbers.iterator();
        while (itr.hasNext())
        {
            System.out.println(itr.next());
        }

    }
}
