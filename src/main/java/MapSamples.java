import java.util.*;

public class MapSamples {
    public static void main(String[] args) {
        Map<Integer,String> map = new HashMap<>();
        map.put(1,"apple");
        map.put(2,"mango");
        map.put(3,"orange");
        map.put(4,"grapes");
        map.put(null,"pineapple");

        map.remove(3);

//        for (HashMap.Entry entry: map.entrySet())
//        {
//            System.out.println("Key is: "+entry.getKey()+" value is: "+entry.getValue());
//        }

        Map<String, List<String>> values = new LinkedHashMap<>();
        values.put("fruits", Arrays.asList("apple","mango"));
        values.put("weeks",Arrays.asList("sunday","monday"));
        values.put("days",Arrays.asList(null,null));
        values.put(null,Arrays.asList(null,null));
        values.put(null,Arrays.asList(null,null));

        for (Map.Entry entry: values.entrySet())
        {
            System.out.println("Key is: "+entry.getKey()+" value is: "+entry.getValue());
        }

        Map<String,Map<Integer,String>> nestedMap = new HashMap<>();




    }
}
