import java.util.LinkedHashSet;
import java.util.Set;

public class LinkedHashSetSample {
    public static void main(String[] args) {
        Set<Integer> set = new LinkedHashSet<>();
        set.add(11);
        set.add(22);
        set.add(13);
        set.add(88);
        set.add(55);
        set.add(11);
        // set.remove(13);

        for (Integer num: set)
        {
            System.out.println(num);
        }
    }
}
